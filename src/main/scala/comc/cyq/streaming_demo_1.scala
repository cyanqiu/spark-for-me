package comc.cyq

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by cyq on 2018/6/21.
  * spark streaming 学习开始
  */
object streaming_demo_1 {

  def main(args: Array[String]): Unit = {
    //在同一段时间内，一个 JVM 上只能运行一个 Spark Context，否则会报错
    val sparkConf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val ssc = new StreamingContext(sparkConf,Seconds(2))
    val lines = ssc.socketTextStream("localhost",9999)
    val words = lines.flatMap(_.split(" "))
    val pairs = words.map(word => (word, 1))
    val wordCounts = pairs.reduceByKey(_ + _)
    wordCounts.print()
    ssc.start()

  }




}
