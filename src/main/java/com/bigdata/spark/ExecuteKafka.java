//package com.bigdata.spark;
//
//import com.bigdata.spark.util.DateUtils;
//import com.bigdata.spark.util.FtpFile;
//import org.apache.commons.lang3.StringUtils;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import redis.clients.jedis.Jedis;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
///**
// * Created by dahey on 2018/3/20.
// */
//public class ExecuteKafka {
//    private HashMap<String, String> params;
//    private static ExecuteKafka instance = null;
//    private SendToKafka sendToKafka;
//    private static Map<String, FtpFile> FtpMap = new HashMap();
//    private Map<String, List<String>> fuzzyRulesCache;
//
//    public ExecuteKafka(HashMap<String, String> params, SendToKafka sendToKafka)
//    {
//        this.params = params;
//        this.sendToKafka = sendToKafka;
//    }
//
//    public void executeData(String topicName, JSONObject info, Jedis jedis, Map<String, List<String>> fuzzyRulesCache)
//            throws Exception
//    {
//        this.fuzzyRulesCache = fuzzyRulesCache;
//
//        accurateMatch(topicName, info, jedis);
//
//        executeQtmatch(topicName, info, jedis);
//
//        pocusPeopleMatch(topicName, info, jedis);
//    }
//
//    private void pocusPeopleMatch(String topicName, JSONObject info, Jedis jedis)
//            throws Exception
//    {
//        jedis.select(Integer.parseInt((String)this.params.get("src.redis.focusdb")));
//        String rowkeyJsonStr = (String)this.params.get("src.kafka.rowkey");
//        JSONObject rowkeyJson = new JSONObject(rowkeyJsonStr);
//
//        String rowkey = rowkeyJson.getString(topicName);
//        if (info.has(rowkey)) {
//            Object redisKeyObject = info.get(rowkey);
//            if ((redisKeyObject instanceof String)) {
//                String redisKey = String.valueOf(redisKeyObject);
//
//                List<String> bks = jedis.lrange(redisKey, 0L, -1L);
//                for (String bk : bks)
//                {
//                    JSONObject judge = new JSONObject(bk);
//                    judge2(jedis, info, redisKey, topicName, judge, MATCH_TYPES.FOCUS_PEOPLE.name(), SEND_TYPE.kafka.name());
//                }
//            }
//        }
//    }
//
//    private void executeQtmatch(String topicName, JSONObject info, Jedis jedis)
//            throws Exception
//    {
//        jedis.select(Integer.parseInt((String)this.params.get("src.redis.db10")));
//        String rowkeyJsonStr = (String)this.params.get("src.kafka.rowkey");
//        JSONObject rowkeyJson = new JSONObject(rowkeyJsonStr);
//
//        String rowkey = rowkeyJson.getString(topicName);
//        if (info.has(rowkey)) {
//            Object redisKeyObject = info.get(rowkey);
//            if ((redisKeyObject instanceof String)) {
//                String redisKey = String.valueOf(redisKeyObject);
//
//                List<String> bks = jedis.lrange(redisKey, 0L, -1L);
//
//                for (String bk : bks)
//                {
//                    JSONObject judge = new JSONObject(bk);
//                    judge(jedis, info, redisKey, topicName, judge, MATCH_TYPES.QT.name(), SEND_TYPE.ftp.name());
//                }
//            }
//        }
//    }
//
//    private void accurateMatch(String topicName, JSONObject info, Jedis jedis)
//            throws Exception
//    {
//        jedis.select(Integer.parseInt((String)this.params.get("src.redis.db")));
//        String rowkeyJsonStr = (String)this.params.get("src.kafka.rowkey");
//        JSONObject rowkeyJson = new JSONObject(rowkeyJsonStr);
//
//        String rowkey = rowkeyJson.getString(topicName);
//        if (info.has(rowkey)) {
//            Object redisKeyObject = info.get(rowkey);
//            if ((redisKeyObject instanceof String)) {
//                String redisKey = String.valueOf(redisKeyObject);
//
//                List<String> bks = jedis.lrange(redisKey, 0L, -1L);
//
//                for (String bk : bks)
//                {
//                    JSONObject judge = new JSONObject(bk);
//                    judge(jedis, info, redisKey, topicName, judge, MATCH_TYPES.DEFAULT.name(), SEND_TYPE.kafka.name());
//                }
//            }
//        }
//    }
//
//    private void fuzzyMatch(String topicName, JSONObject info, Jedis jedis)
//            throws Exception
//    {
//        String rowkeyJsonStr = (String)this.params.get("src.kafka.rowkey");
//        JSONObject rowkeyJson = new JSONObject(rowkeyJsonStr);
//
//        String rowkey = rowkeyJson.getString(topicName);
//
//        if (info.has(rowkey)) {
//            Object redisKeyObject = info.get(rowkey);
//            if ((redisKeyObject instanceof String)) {
//                String redisKey = String.valueOf(redisKeyObject);
//                Iterator localIterator2;
//                for (Iterator localIterator1 = this.fuzzyRulesCache.entrySet().iterator(); localIterator1.hasNext();
//                     localIterator2.hasNext())
//                {
//                    Map.Entry entry = (Map.Entry)localIterator1.next();
//                    String key = (String)entry.getKey();
//                    List bks = (List)entry.getValue();
//                    localIterator2 = bks.iterator();
////                    continue;
//                    String bk = (String)localIterator2.next();
//
//                    JSONObject judge = new JSONObject(bk);
//                    if (judge.has("REG")) {
//                        String reg = judge.getString("REG");
//                        if (isReg(reg, redisKey))
//                            judge(jedis, info, redisKey, topicName, judge, MATCH_TYPES.DEFAULT.name(), SEND_TYPE.kafka.name());
//                    }
//                }
//            }
//        }
//    }
//
//    private void judge(Jedis jedis, JSONObject src, String redisKey, String topicName, JSONObject judge, String matchType, String sendType)
//            throws Exception
//    {
//        String srcdd = new JSONObject((String)this.params.get("src.kafka.dd")).getString(topicName);
//        String srcsj = new JSONObject((String)this.params.get("src.kafka.sj")).getString(topicName);
//        String srcqt = new JSONObject((String)this.params.get("src.kafka.qt")).getString(topicName);
//
//        if (!judge.has("ID")) {
//            return;
//        }
//
//        String topicStr = judge.getString("GZSJ");
//        if (topicStr.indexOf(topicName) == -1) {
//            return;
//        }
//
//        String gzId = judge.getString("ID");
//
//        String xh = judge.getString("XH");
//
//        JSONArray gzs = judge.getJSONArray("GZS");
//        if (StringUtils.isNotBlank(xh)) {
//            JSONObject gz = gzs.getJSONObject(Integer.parseInt(xh));
//
//            if ((gz.has("SHT")) && (!gz.getString("SHT").isEmpty()) && (
//                    (!src.has(srcsj)) || (gz.getString("SHT").compareTo(src.getString(srcsj).substring(8)) > 0))) {
//                return;
//            }
//            if ((gz.has("EHT")) && (!gz.getString("EHT").isEmpty()) && (
//                    (!src.has(srcsj)) || (gz.getString("EHT").compareTo(src.getString(srcsj).substring(8)) < 0))) {
//                return;
//            }
//
//            if ((gz.has("DD")) && (!gz.getString("DD").isEmpty()) &&
//                    (!isSpecialAddress(topicName, jedis, srcdd, gz.getString("DD"), src))) return;
//
//            if ((gz.has("MZ")) && (!gz.getString("MZ").isEmpty()) && (
//                    (!src.has("MZ")) || (!gz.getString("MZ").equals(src.getString("MZ"))))) {
//                return;
//            }
//
//            if ((gz.has("QT")) && (!gz.getString("QT").isEmpty()) && (!gz.getString("QT").equals("-")) && (
//                    (!src.has(srcqt)) || (!gz.getString("QT").equals(src.getString(srcqt))))) {
//                return;
//            }
//
//            if ((gz.has("CTS")) && (!gz.getString("CTS").isEmpty()))
//            {
//                jedis.select(Integer.parseInt((String)this.params.get("src.redis.cache")));
//
//                String key = redisKey + "_" + gzId + "_" + xh;
//
//                jedis.incr(key);
//                String counts = jedis.get(key);
//
//                jedis.select(Integer.parseInt((String)this.params.get("src.redis.data")));
//
//                if (gz.getString("CTS").compareTo(counts) <= 0) {
//                    jedis.set(key, "TRUE");
//                }
//
//                boolean flag = true;
//                for (int i = 0; i < gzs.length(); i++) {
//                    if ((jedis.get(redisKey + "_" + gzId + "_" + i) == null) ||
//                            (!jedis.get(redisKey + "_" + gzId + "_" + i).equals("TRUE"))) {
//                        flag = false;
//                        break;
//                    }
//                }
//                if (!flag) {
//                    return;
//                }
//
//                jedis.select(Integer.parseInt((String)this.params.get("src.redis.cache")));
//                for (int i = 0; i < gzs.length(); i++) {
//                    src.put(redisKey + "_" + gzId + "_" + i, jedis.get(redisKey + "_" + gzId + "_" + i));
//                }
//            }
//        }
//
//        String sendeeValue = "";
//        if (judge.has("SENDEEVALUE")) {
//            sendeeValue = judge.getString("SENDEEVALUE");
//        }
//
//        src = mergeParams(src, judge);
//
//        src = mergeData(topicName, src, jedis);
//
//        src.put("MATCHDATE", DateUtils.getCurrDate("yyyyMMddHHmmss"));
//        src.put("KEYWORDS", redisKey);
//        src.put("GJLX", topicName);
//
//        String sendTopic = new JSONObject((String)this.params.get("dst.kafka.topic")).getString(topicName);
//        if (matchType.equals("DEFAULT")) {
//            src.put("ID", judge.getString("ID"));
//            src.put("SENDEENAME", judge.getString("SENDEENAME"));
//            src.put("SENDEEVALUE", sendeeValue);
//            src.put("SENDEETYPE", judge.getString("SENDEETYPE"));
//            src.put("TYPE", judge.getString("TYPE"));
//            src.put("DBID", judge.get("DBID").toString());
//        }
//
//        new TypeConsumerHandlerThread(this.params, sendType, topicName, sendTopic, src.toString(), this.sendToKafka).run();
//    }
//
//    private void judge2(Jedis jedis, JSONObject src, String redisKey, String topicName, JSONObject judge, String matchType, String sendType)
//            throws Exception
//    {
//        String srcdd = new JSONObject((String)this.params.get("src.kafka.dd")).getString(topicName);
//
//        if (!judge.has("ID")) {
//            return;
//        }
//
//        String topicStr = judge.getString("GZSJ");
//        if (topicStr.indexOf(topicName) == -1) {
//            return;
//        }
//
//        Map ppddMap = JsonUtils.parseToMap((String)this.params.get("src.focus.people.dd"));
//        for (Map.Entry entry : ppddMap.entrySet()) {
//            String ppdd = (String)entry.getKey();
//            String sendTopic = (String)entry.getValue();
//
//            if (isSpecialAddress(topicName, jedis, srcdd, ppdd, src))
//            {
//                src = mergeParams(src, judge);
//
//                src = mergeData(topicName, src, jedis);
//
//                src.put("MATCHDATE", DateUtils.getCurrDate("yyyyMMddHHmmss"));
//                src.put("KEYWORDS", redisKey);
//                src.put("GJLX", topicName);
//
//                new TypeConsumerHandlerThread(this.params, sendType, topicName, sendTopic, src.toString(), this.sendToKafka).run();
//            }
//        }
//    }
//
//    public JSONObject mergeParams(JSONObject src, JSONObject judge)
//    {
//        if (judge.has("PARAMS")) {
//            JSONObject paramsObj = new JSONObject(judge.getString("PARAMS"));
//            Iterator it = paramsObj.keys();
//            while (it.hasNext()) {
//                String key = it.next().toString();
//                if (!key.trim().equals(""))
//                    src.put(key, paramsObj.get(key));
//            }
//        }
//        return src;
//    }
//
//    public JSONObject mergeData(String topicName, JSONObject src, Jedis jedis)
//    {
//        if (topicName.equals("ST_MH_JGXXB"))
//        {
//            String rowKey = src.getString("SEG_DEPT_CODE");
//            JSONObject valueJson = getMHDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("SFDMC", valueJson.getString("FJCMC"));
//                src.put("SFDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//
//            rowKey = src.getString("SEG_DEST_CODE");
//            valueJson = getMHDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("MDDMC", valueJson.getString("FJCMC"));
//                src.put("MDDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//
//        }
//        else if (topicName.equals("ST_MH_DZXXB"))
//        {
//            String rowKey = src.getString("AIR_SEG_DPT_AIRPT_CD");
//            JSONObject valueJson = getMHDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("SFDMC", valueJson.getString("FJCMC"));
//                src.put("SFDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//
//            rowKey = src.getString("AIR_SEG_ARRV_AIRPT_CD");
//            valueJson = getMHDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("MDDMC", valueJson.getString("FJCMC"));
//                src.put("MDDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//
//        }
//        else if (topicName.equals("ST_TL_LKSMDP"))
//        {
//            String rowKey = src.getString("FZ");
//            JSONObject valueJson = getHCDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("SFDMC", src.getString("FZ"));
//                src.put("SFDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//
//            rowKey = src.getString("DZ");
//            valueJson = getHCDICT(rowKey, jedis);
//            if (valueJson != null) {
//                src.put("MDDMC", src.getString("DZ"));
//                src.put("MDDXZQHOOO", valueJson.getString("XZQHOOO"));
//            }
//        }
//
//        return src;
//    }
//
//    public JSONObject getMHDICT(String rowKey, Jedis jedis)
//    {
//        JSONObject valueJson = null;
//        jedis.select(Integer.parseInt((String)this.params.get("src.redis.mhdb")));
//        List values = jedis.lrange(rowKey, 0L, -1L);
//        if ((values != null) && (values.size() != 0)) {
//            valueJson = new JSONObject((String)values.get(0));
//        }
//        return valueJson;
//    }
//
//    public JSONObject getHCDICT(String rowKey, Jedis jedis)
//    {
//        JSONObject valueJson = null;
//        jedis.select(Integer.parseInt((String)this.params.get("src.redis.hcdb")));
//        List values = jedis.lrange(rowKey, 0L, -1L);
//        if ((values != null) && (values.size() != 0)) {
//            valueJson = new JSONObject((String)values.get(0));
//        }
//        return valueJson;
//    }
//
//    private boolean isDefaultAddress(String ddStr, JSONObject gz, JSONObject data)
//    {
//        boolean falg = false;
//        String[] dds = ddStr.split("&");
//        for (String dd : dds) {
//            if ((data.has(dd)) &&
//                    (data.getString(dd).equals(gz.getString("DD")))) {
//                falg = true;
//                break;
//            }
//        }
//
//        return falg;
//    }
//
//    private boolean isSpecialAddress(String topicName, Jedis jedis, String ddStr, String ddGz, JSONObject data)
//    {
//        boolean falg = false;
//        String[] dds = ddStr.split("&");
//        for (String dd : dds) {
//            if (data.has(dd)) {
//                String dataDd = data.getString(dd);
//                if ((topicName.equals("ST_MH_JGXXB")) || (topicName.equals("ST_MH_DZXXB"))) {
//                    JSONObject valueJson = getMHDICT(dataDd, jedis);
//                    if (valueJson != null) {
//                        String xzqhooo = valueJson.getString("XZQHOOO");
//                        if (xzqhooo.startsWith(ddGz)) {
//                            falg = true;
//                            break;
//                        }
//                    }
//                }
//                else if (topicName.equals("ST_TL_LKSMDP")) {
//                    JSONObject valueJson = getHCDICT(dataDd, jedis);
//                    if (valueJson != null) {
//                        String xzqhooo = valueJson.getString("XZQHOOO");
//                        if (xzqhooo.startsWith(ddGz)) {
//                            falg = true;
//                            break;
//                        }
//                    }
//
//                }
//                else if (dataDd.startsWith(ddGz)) {
//                    falg = true;
//                    break;
//                }
//            }
//        }
//
//        return falg;
//    }
//
//    private static boolean isReg(String reg, String input)
//    {
//        boolean isReg = false;
//        Pattern p = Pattern.compile(reg);
//        Matcher m = p.matcher(input);
//        while (m.find()) {
//            int start = m.start();
//            int end = m.end();
//            String out = input.substring(start, end);
//            if (out.equals(input)) {
//                isReg = true;
//            }
//        }
//        return isReg;
//    }
//
//    public static void main(String[] args)
//    {
//        System.out.println(isReg("51072[0-9]{12}([0-9]|X|x){1}", "510726198301045121"));
//    }
//
//    public static enum MATCH_TYPES
//    {
//        DEFAULT,
//        FOCUS_PEOPLE,
//        QT;
//    }
//
//    public static enum SEND_TYPE
//    {
//        kafka,
//        ftp;
//    }
//}
