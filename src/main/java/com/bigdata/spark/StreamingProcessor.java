//package com.bigdata.spark;
//
//import com.bigdata.spark.util.Config;
//import com.bigdata.spark.util.RedisClient;
//import com.google.gson.JsonObject;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.Function;
//import org.apache.spark.api.java.function.VoidFunction;
//import org.apache.spark.broadcast.Broadcast;
//import org.apache.spark.streaming.Durations;
//import org.apache.spark.streaming.api.java.JavaDStream;
//import org.apache.spark.streaming.api.java.JavaInputDStream;
//import org.apache.spark.streaming.api.java.JavaStreamingContext;
//import org.apache.spark.streaming.kafka010.ConsumerStrategies;
//import org.apache.spark.streaming.kafka010.KafkaUtils;
//import org.apache.spark.streaming.kafka010.LocationStrategies;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.exceptions.JedisException;
//
//import java.util.*;
//
//public class StreamingProcessor
//{
//    public static void main(String[] args)
//    {
//        Properties properties = Config.getInstance().getP();
//
//        SparkConf conf = new SparkConf().setMaster(properties.getProperty("master")).setAppName(properties.getProperty("src.kafka.topic"));
//        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
//        conf.set("spark.kryo.registrator", "com.unimas.spark.util.MyRegistrator");
//        conf.set("spark.executor.extraJavaOptions", "-XX:+UseConcMarkSweepGC");
//        conf.set("spark.streaming.kafka.consumer.poll.ms", "100000");
//
//        JavaSparkContext sc = new JavaSparkContext(conf);
//        sc.setLogLevel("WARN");
//
//        JavaStreamingContext ssc = new JavaStreamingContext(sc,
//                Durations.seconds(Integer.parseInt(properties.getProperty("duration"))));
//
//        String topicstr = properties.getProperty("src.kafka.topic");
//        String[] srcTopics = topicstr.split(",");
//        Collection topicSet = new HashSet(Arrays.asList(srcTopics));
//
//        Map kafkaParams = new HashMap();
//
//        kafkaParams.put("bootstrap.servers", properties.getProperty("src.kafka.brokers"));
//
//        kafkaParams.put("key.deserializer", StringDeserializer.class);
//        kafkaParams.put("value.deserializer", StringDeserializer.class);
//        kafkaParams.put("group.id", properties.getProperty("src.kafka.group"));
//        kafkaParams.put("auto.offset.reset", properties.getProperty("src.kafka.auto.offset.reset"));
//        kafkaParams.put("enable.auto.commit", Boolean.valueOf(false));
//
//        final HashMap params = new HashMap();
//        params.put("topicstr", topicstr);
//        try
//        {
//            params.put("dst.kafka.brokers", properties.getProperty("dst.kafka.brokers"));
//
//            String dstTopicstr = properties.getProperty("dst.kafka.topic");
//            String[] dstTopics = dstTopicstr.split(",");
//            JsonObject dstTopicJson = new JsonObject();
//            for (int i = 0; i < srcTopics.length; i++) {
//                dstTopicJson.addProperty(srcTopics[i], dstTopics[i]);
//            }
//            params.put("dst.kafka.topic", dstTopicJson.toString());
//            params.put("dst.kafka.client.id",
//                    properties.getProperty("dst.kafka.client.id"));
//
//            String rowkeystr = properties.getProperty("src.kafka.rowkey");
//            String[] rowkeys = rowkeystr.split(",");
//            JsonObject rowkeyJson = new JsonObject();
//            for (int i = 0; i < srcTopics.length; i++) {
//                rowkeyJson.addProperty(srcTopics[i], rowkeys[i]);
//            }
//            params.put("src.kafka.rowkey", rowkeyJson.toString());
//
//            String ddstr = properties.getProperty("src.kafka.dd");
//            String[] dds = ddstr.split(",");
//            JsonObject ddJson = new JsonObject();
//            for (int i = 0; i < srcTopics.length; i++) {
//                ddJson.addProperty(srcTopics[i], dds[i]);
//            }
//            params.put("src.kafka.dd", ddJson.toString());
//
//            String sjstr = properties.getProperty("src.kafka.sj");
//            String[] sjs = sjstr.split(",");
//            JsonObject sjJson = new JsonObject();
//            for (int i = 0; i < srcTopics.length; i++) {
//                sjJson.addProperty(srcTopics[i], sjs[i]);
//            }
//            params.put("src.kafka.sj", sjJson.toString());
//
//            String qtstr = properties.getProperty("src.kafka.qt");
//            String[] qts = qtstr.split(",");
//            JsonObject qtJson = new JsonObject();
//            for (int i = 0; i < srcTopics.length; i++) {
//                qtJson.addProperty(srcTopics[i], qts[i]);
//            }
//            params.put("src.kafka.qt", qtJson.toString());
//
//            params.put("src.redis.host", properties.getProperty("src.redis.host"));
//            params.put("src.redis.port", properties.getProperty("src.redis.port"));
//            params.put("src.redis.db", properties.getProperty("src.redis.db"));
//            params.put("src.redis.db10", properties.getProperty("src.redis.db10"));
//            params.put("src.redis.likedb", properties.getProperty("src.redis.likedb"));
//            params.put("src.redis.cache", properties.getProperty("src.redis.cache"));
//            params.put("src.redis.data", properties.getProperty("src.redis.data"));
//            params.put("src.redis.repeat", properties.getProperty("src.redis.repeat"));
//            params.put("empty.redis4.time", properties.getProperty("empty.redis4.time"));
//            params.put("empty.redis23.time", properties.getProperty("empty.redis23.time"));
//            params.put("src.redis.mhdb", properties.getProperty("src.redis.mhdb"));
//            params.put("src.redis.hcdb", properties.getProperty("src.redis.hcdb"));
//
//            params.put("src.redis.focusdb", properties.getProperty("src.redis.focusdb"));
//            params.put("src.focus.people.dd", properties.getProperty("src.focus.people.dd"));
//            params.put("dst.kafka.focus_people.topic", properties.getProperty("dst.kafka.focus_people.topic"));
//
//            String[] fpds = ((String)params.get("src.focus.people.dd")).split(",");
//            String[] dkfpts = ((String)params.get("dst.kafka.focus_people.topic")).split(",");
//            JsonObject fpdJson = new JsonObject();
//            for (int i = 0; i < fpds.length; i++) {
//                fpdJson.addProperty(fpds[i], dkfpts[i]);
//            }
//            params.put("src.focus.people.dd", fpdJson.toString());
//
//            params.put("ftp.url", properties.getProperty("ftp.url"));
//            params.put("ftp.port", properties.getProperty("ftp.port"));
//            params.put("ftp.username", properties.getProperty("ftp.username"));
//            params.put("ftp.password", properties.getProperty("ftp.password"));
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        RedisClient redisClient = RedisClient.getInstance(properties.getProperty("src.redis.host"),
//                Integer.parseInt(properties.getProperty("src.redis.port")));
//
//        final Broadcast broadcastRedis = sc.broadcast(redisClient);
//        Broadcast broadcastParams = sc.broadcast(params);
//        Broadcast broadcastTopicSet = sc.broadcast(topicSet);
//
//        SendToKafka sendToKafka = SendToKafka.getInstance((HashMap)broadcastParams.value());
//        Broadcast broadcastSendToKafka = sc.broadcast(sendToKafka);
//
//        ExecuteKafka executeKafka = new ExecuteKafka((HashMap)broadcastParams.value(), (SendToKafka)broadcastSendToKafka.value());
//        Broadcast broadcastExcuteKafka = sc.broadcast(executeKafka);
//
//
//        JavaDStream lines = stream.filter(new Function()
//        {
//            public Boolean call(ConsumerRecord<String, String> arg0) throws Exception
//            {
//                try {
//                    if ((arg0 == null) || (arg0.value() == null) || (((String)arg0.value()).equals(""))) return Boolean.valueOf(false);
//                    new JSONObject((String)arg0.value());
//                } catch (JSONException e) {
//                    return Boolean.valueOf(false);
//                }
//                return Boolean.valueOf(true);
//            }
//        }).map(new Function()
//        {
//            public String call(ConsumerRecord<String, String> arg0)
//                    throws Exception
//            {
//                return (String)arg0.value();
//            }
//        });
//        JavaInputDStream stream =
//                KafkaUtils.createDirectStream(
//                        ssc,
//                        LocationStrategies.PreferConsistent(),
//                        ConsumerStrategies.Subscribe(topicSet, kafkaParams));
//        lines.foreachRDD(new VoidFunction()
//        {
//            public void call(JavaRDD<String> rdd) throws Exception
//            {
//                rdd.foreachPartition(new VoidFunction()
//                {
//                    public void call(Iterator<String> it) throws Exception
//                    {
//                        if (it.hasNext()) {
//                            ExecuteKafka executeKafka = (ExecuteKafka)this.val$broadcastExcuteKafka.getValue();
//                            RedisClient redisClient = (RedisClient)this.val$broadcastRedis.getValue();
//                            Jedis jedis = null;
//                            boolean isSuccess = true;
//                            try {
//                                jedis = redisClient.getResource();
//
//                                Map fuzzyRules =
//                                        getFuzzyRules(
//                                                (RedisClient)this.val$broadcastRedis.value(),
//                                                jedis,
//                                                Integer.parseInt((String)this.val$params.get("src.redis.likedb")));
//
//                                while (it.hasNext()) {
//                                    String message = (String)it.next();
//
//                                    JSONObject kafkaData = new JSONObject(message);
//                                    String topic = kafkaData.keys().next().toString();
//                                    Object obj = kafkaData.get(topic);
//                                    if ((obj instanceof JSONArray)) {
//                                        JSONArray infos = (JSONArray)obj;
//                                        if (infos.length() != 0) {
//                                            JSONObject info = infos.getJSONObject(0);
//                                            executeKafka.executeData(topic, info, jedis, fuzzyRules);
//                                        }
//                                    }
//                                }
//                            }
//                            catch (JedisException e)
//                            {
//                                isSuccess = false;
//                                redisClient.returnBrokenResource(jedis);
//                                e.printStackTrace();
//                            } finally {
//                                if (isSuccess) {
//                                    redisClient.returnResource(jedis);
//                                }
//                            }
//                        }
//                    }
//                });
//            }
//
//        });
//        try
//        {
//            ssc.start();
//            ssc.awaitTermination();
//            ssc.close();
//        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static Map<String, List<String>> getFuzzyRules(RedisClient redisClient, Jedis jedis, int db)
//    {
//        Map fuzzyRulesMap = new HashMap();
//        jedis.select(db);
//        Set<String> keys = jedis.keys("*");
//        for (String key : keys) {
//            fuzzyRulesMap.put(key, jedis.lrange(key, 0L, -1L));
//        }
//        return fuzzyRulesMap;
//    }
//}