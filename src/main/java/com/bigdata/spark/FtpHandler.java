package com.bigdata.spark;

import com.bigdata.spark.util.DateUtils;
import com.bigdata.spark.util.FTPUtil;
import com.bigdata.spark.util.FtpFile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by dahey on 2018/3/20.
 */
public class FtpHandler extends ConsumerHandler {
//    Logger logger = Logger.getLogger(FtpHandler.class);

    private static final Lock lock = new ReentrantLock();

    public static Map<String, FtpFile> FTP_FILE_MAP = new HashMap();
    private String host;
    private int port = 21;
    private String userName;
    private String password;

    public FtpHandler(HashMap<String, String> params, String srcTopic, String message)
    {
        super(params, srcTopic, message);
        init();
    }

    protected Long coreHandler()
    {
        boolean seccess = sendMessage();
        if (seccess) {
            return Long.valueOf(1L);
        }
        return Long.valueOf(0L);
    }

    private void init()
    {
        this.host = ((String)this.params.get("ftp.url")).toString();
        this.port = Integer.parseInt(((String)this.params.get("ftp.port")).toString());
        this.userName = ((String)this.params.get("ftp.username")).toString();
        this.password = ((String)this.params.get("ftp.password")).toString();
    }

    private boolean sendMessage()
    {
        lock.lock();
        boolean success = true;
        try
        {
            String key = "SC_" + this.dstTopic;

            FtpFile ftpFile = getFtpFile(key);

            FTPUtil ftpClient = FTPUtil.getInstance();
            ftpClient.open(this.host, this.port, this.userName, this.password);
            ftpClient.writeFile(ftpFile.getFileName() + ".txt",
                    ftpFile.getGjlx(),
                    this.message.toString());
            ftpClient.close();
        }
        catch (Exception e)
        {
            success = false;
            e.printStackTrace();
        } finally {
            lock.unlock();
        }return success;
    }

    private FtpFile getFtpFile(String key)
            throws Exception
    {
        FtpFile ftpFile = null;
        if (FTP_FILE_MAP.containsKey(key)) {
            ftpFile = (FtpFile)FTP_FILE_MAP.get(key);
            String nowDate = DateUtils.formatDate(new Date(), "yyyyMMdd");
            String fileName = ftpFile.getFileName();
            if (!nowDate.equals(fileName)) {
                ftpFile.setFileName(DateUtils.formatDate(new Date(), "yyyyMMdd"));
                FTP_FILE_MAP.put(key, ftpFile);
            }
        }
        else {
            ftpFile = new FtpFile();
            ftpFile.setGjlx(this.dstTopic);
            ftpFile.setFileName(DateUtils.formatDate(new Date(), "yyyyMMdd"));
            FTP_FILE_MAP.put(key, ftpFile);
        }
        return ftpFile;
    }
}
