//package com.bigdata.spark.util;
//
//import com.google.gson.*;
//import com.google.gson.reflect.TypeToken;
//import org.apache.log4j.Logger;
//import org.codehaus.jackson.map.DeserializationConfig;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.map.annotate.JsonSerialize;
//import org.codehaus.jackson.type.TypeReference;
//
//import java.io.*;
//import java.lang.reflect.Type;
//import java.text.SimpleDateFormat;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
///**
// * Created by dahey on 2018/3/21.
// */
//public class JsonUtils {
//    private static final Logger logger = Logger.getLogger(JsonUtils.class);
//
//    private static ObjectMapper MAPPER = generateMapper(JsonSerialize.Inclusion.ALWAYS);
//
//    public static String toJsonWithoutExposeAnnotation(Object obj)
//    {
//        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//        return gson.toJson(obj);
//    }
//
//    public static String toJson(Object obj)
//    {
//        Gson gson = new Gson();
//        return gson.toJson(obj);
//    }
//
//    public static Map<String, String> parseToMap(String json)
//    {
//        Type type = new TypeToken() {  } .getType();
//        Gson gson = new Gson();
//        Map o = (Map)gson.fromJson(json, type);
//        return o;
//    }
//
//    public static <T> T parse(String json, Class<T> cla)
//    {
//        Gson gson = new Gson();
//        Object o = gson.fromJson(json, cla);
//        return o;
//    }
//
//    public static JsonArray objToJsonArray(List list)
//    {
//        String json = toJson(list);
//        JsonParser jsonParser = new JsonParser();
//        JsonArray jsonArray = jsonParser.parse(json).getAsJsonArray();
//        return jsonArray;
//    }
//
//    public static JsonObject objToJsonObject(Object object)
//    {
//        String json = toJson(object);
//        JsonParser jsonParser = new JsonParser();
//        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();
//        return jsonObject;
//    }
//
//    public static String toJson2(String dataformat, Object obj)
//    {
//        Gson gson = new GsonBuilder().setDateFormat(dataformat).create();
//        return gson.toJson(obj);
//    }
//
//    public static Gson getGson()
//    {
//        Gson gson = new GsonBuilder().registerTypeAdapter(HashMap.class,
//                new JsonDeserializer()
//                {
//                    public HashMap deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2)
//                            throws JsonParseException
//                    {
//                        HashMap resultMap = new HashMap();
//                        JsonObject jsonObject = arg0.getAsJsonObject();
//                        Set entrySet = jsonObject
//                                .entrySet();
//                        for (Map.Entry entry : entrySet) {
//                            JsonElement value = (JsonElement)entry.getValue();
//                            resultMap.put(entry.getKey(), entry.getValue());
//                        }
//                        return resultMap;
//                    }
//                }).create();
//        return gson;
//    }
//
//    public static JsonObject strToJsonObject(String str)
//    {
//        JsonParser jp = new JsonParser();
//        JsonObject d = jp.parse(str).getAsJsonObject();
//        return d;
//    }
//
//    public static JsonArray strToJsonArray(String str)
//    {
//        JsonParser jp = new JsonParser();
//        JsonArray d = jp.parse(str).getAsJsonArray();
//        return d;
//    }
//
//    public static void writeObjToFile(File jsonFile, Object pojo)
//            throws IOException
//    {
//        Gson g = new Gson();
//        String json = g.toJson(pojo);
//        writeStrToFile(jsonFile, json);
//    }
//
//    public static void writeStrToFile(File jsonFile, String json) throws IOException {
//        BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jsonFile), "utf-8"));
//        String str = jsonFormatter(json);
//        fw.write(str);
//        fw.flush();
//        fw.close();
//    }
//
//    public static <T> T getObjFromFile(String path, Class<T> clas)
//            throws IOException
//    {
//        File file = new File(path);
//        return getObjFromFile(file, clas);
//    }
//
//    public static <T> T getObjFromFile(File jsonFile, Class<T> clas)
//            throws IOException
//    {
//        return getObjFromFile(jsonFile, clas, "utf-8");
//    }
//
//    public static <T> T getObjFromFile(File jsonFile, Type t) throws IOException {
//        return getObjFromFile(jsonFile, t, "utf-8");
//    }
//
//    public static <T> T getObjFromFile(File jsonFile, Class<T> clas, String encoding)
//            throws IOException
//    {
//        String str = getStringFromFile(jsonFile, encoding);
//        Gson g = new Gson();
//        return g.fromJson(str, clas);
//    }
//
//    public static <T> T getObjFromFile(File jsonFile, Type t, String encoding) throws IOException {
//        String str = getStringFromFile(jsonFile, encoding);
//        Gson g = new Gson();
//        return g.fromJson(str, t);
//    }
//
//    public static String getStringFromFile(File jsonFile, String encoding) throws IOException {
//        StringBuffer sb = new StringBuffer();
//
//        BufferedReader fr = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFile), encoding));
//        String str = fr.readLine();
//        while (str != null) {
//            sb.append(str);
//            str = fr.readLine();
//        }
//        fr.close();
//
//        return sb.toString();
//    }
//
//    public static String jsonFormatter(String json) {
//        Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        JsonParser jp = new JsonParser();
//        JsonElement je = jp.parse(json);
//        String str = gson.toJson(je);
//        return str;
//    }
//
//    public static <T> T fromJson(String json, Class<T> clazz)
//    {
//        try
//        {
//            return clazz.equals(String.class) ? json : MAPPER.readValue(json, clazz);
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return null;
//    }
//
//    public static <T> T fromJson(FileReader jsonFileReader, Class<T> clazz) {
//        try {
//            return MAPPER.readValue(jsonFileReader, clazz);
//        } catch (Exception e) {
//            logger.error("error json is : " + jsonFileReader.toString());
//        }
//        return null;
//    }
//
//    public static <T> T fromJson(InputStreamReader jsonFileReader, Class<T> clazz) {
//        try {
//            return MAPPER.readValue(jsonFileReader, clazz);
//        } catch (Exception e) {
//            logger.error("error json is : " + jsonFileReader.toString());
//        }
//        return null;
//    }
//
//    public static Map<String, Object> fromJson(String line) {
//        try {
//            return (Map)MAPPER.readValue(line, Map.class);
//        } catch (Exception e) {
//            logger.error("error json is : " + line);
//        }
//        return null;
//    }
//
//    public static <T> T fromJson(String json, TypeReference<?> typeReference)
//    {
//        try
//        {
//            return typeReference.getType().equals(String.class) ? json : MAPPER.readValue(
//                    json, typeReference);
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return null;
//    }
//
//    public static <T> String toJson2(T src)
//    {
//        try
//        {
//            return (src instanceof String) ? (String)src : MAPPER.writeValueAsString(src);
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return null;
//    }
//
//    public static <T> String toJson(T src, JsonSerialize.Inclusion inclusion)
//    {
//        try
//        {
//            if ((src instanceof String)) {
//                return (String)src;
//            }
//            ObjectMapper customMapper = generateMapper(inclusion);
//            return customMapper.writeValueAsString(src);
//        }
//        catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return null;
//    }
//
//    public static <T> String toJson(T src, ObjectMapper mapper)
//    {
//        try
//        {
//            if (mapper != null) {
//                if ((src instanceof String)) {
//                    return (String)src;
//                }
//                return mapper.writeValueAsString(src);
//            }
//
//            return null;
//        }
//        catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        return null;
//    }
//
//    public static ObjectMapper mapper()
//    {
//        return MAPPER;
//    }
//
//    private static ObjectMapper generateMapper(JsonSerialize.Inclusion inclusion)
//    {
//        ObjectMapper customMapper = new ObjectMapper();
//
//        customMapper.setSerializationInclusion(inclusion);
//
//        customMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//
//        customMapper.configure(DeserializationConfig.Feature.FAIL_ON_NUMBERS_FOR_ENUMS, true);
//
//        customMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
//
//        return customMapper;
//    }
//}
