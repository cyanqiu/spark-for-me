package com.bigdata.spark.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Created by dahey on 2018/3/20.
 */
public class Config {
    private static final Logger logger = Logger.getLogger(Config.class);
    private static final String CONFIG_PROPERTIES_FILE_NAME = "job.config";
    private Properties p;
    private static Config instance = null;

    private Config() {
        init();
    }

    public static Config getInstance()
    {
        if (instance == null) {
            synchronized (Config.class) {
                if (instance == null) {
                    instance = new Config();
                }
            }
        }
        return instance;
    }

    private void init() {
        File pf = new File("job.config");
        logger.info("config_file_path=" + pf.getAbsolutePath());
        FileInputStream inpf = null;
        try {
            inpf = new FileInputStream(pf);
        } catch (Exception e) {
            logger.error("create job.config stream error : " + e.getMessage());
        }
        this.p = new Properties();
        try {
            this.p.load(inpf);
        } catch (Exception e) {
            logger.error("load job.config error : " + e.getMessage());
        }
    }

    public Properties getP() {
        return this.p;
    }

    public static void print(Properties pro)
    {
        System.out.println("###########################################");
        for (Map.Entry entry : pro.entrySet())
        {
            System.out.format("key=%-29s value=[%s] %n", new Object[] { entry.getKey(), entry.getValue() });
        }
        System.out.println("###########################################");
    }
}
