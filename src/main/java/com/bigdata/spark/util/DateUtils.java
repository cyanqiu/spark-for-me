package com.bigdata.spark.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;
/**
 * Created by dahey on 2018/3/20.
 */
public class DateUtils {
    public static final String yyyy = "yyyy";
    public static final String yyyy_MM = "yyyy-MM";
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String MM_dd = "MM-dd";
    public static final String HHmmss = "HH:mm:ss";
    public static final String YYYY_MM_DDHHmmss = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DDTHHmmss = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String YYYY_MM_DDHHmmssSSS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String yyyy_MM_ddTHHmmssSSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final SimpleDateFormat DATE_FORMAT_YYYY = new SimpleDateFormat("yyyy");
    public static final SimpleDateFormat DATE_FORMAT_MM_DD = new SimpleDateFormat("MM-dd");
    public static final SimpleDateFormat DATE_FORMAT_YYYY_MM = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat DATE_FORMAT_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DATE_FORMAT_HH_MM_SS = new SimpleDateFormat("HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_YYYY_MM_DDkgHHmhMMmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final String UDB_DATE_PATTERN = "\\d{4}-\\d{2}-\\d{2}[T|\\s]\\d{2}:\\d{2}:\\d{2}.\\d{1,3}Z?";

    public static String getYesterday()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(5, -1);
        return DATE_FORMAT_YYYY_MM_DD.format(calendar.getTime());
    }

    public static String formatUdbaDate(java.util.Date date)
    {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String formatDate(java.util.Date date, String format)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static java.util.Date getCurrDate()
    {
        return Calendar.getInstance().getTime();
    }

    public static String getCurrDate(String format)
    {
        return new SimpleDateFormat(format).format(Calendar.getInstance().getTime());
    }

    public static java.util.Date getPrevDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(5, -1);
        return calendar.getTime();
    }

    public static java.util.Date getPrevDate(java.util.Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(5, -1);
        return calendar.getTime();
    }

    public static java.util.Date getNextDate(java.util.Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(5, 1);
        return calendar.getTime();
    }

    public static String getNextDate(String date)
    {
        if ((date == null) || (date.length() != 10)) {
            throw new IllegalArgumentException("传入的日期格式不正确：\"" + date + "\"");
        }

        String[] array = date.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(array[0]), Integer.parseInt(array[1]) - 1, Integer.parseInt(array[2]));
        calendar.add(5, 1);

        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    public static String getPrevDate(String format)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(5, -1);
        return new SimpleDateFormat(format).format(calendar.getTime());
    }

    public static java.util.Date getStringToDate(String str, String format)
            throws ParseException
    {
        if ((str == null) || ("".equals(str))) {
            return null;
        }
        return new SimpleDateFormat(format).parse(str);
    }

    public static int getDateField(int feild)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date());
        return cal.get(feild);
    }

    public static java.util.Date getMaxDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1, 9999);
        calendar.set(2, 11);
        calendar.set(5, 31);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        return calendar.getTime();
    }

    public static String getCurrYear()
    {
        return DATE_FORMAT_YYYY.format(Calendar.getInstance().getTime());
    }

    public static String getCurrYear(int point)
    {
        Calendar calendar = Calendar.getInstance();

        if (calendar.get(5) > point) {
            calendar.add(2, 1);
        }

        return DATE_FORMAT_YYYY.format(calendar.getTime());
    }

    public static java.util.Date getPrevMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(2, -1);
        return calendar.getTime();
    }

    public static java.util.Date getPrevYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(1, -1);
        return calendar.getTime();
    }

    public static String getPrevYearStr()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(1, -1);
        return DATE_FORMAT_YYYY.format(calendar.getTime());
    }

    public static int getBetweenDay(String beginDate, String endDate)
    {
        long beginTime = java.sql.Date.valueOf(beginDate).getTime();
        long endTime = java.sql.Date.valueOf(endDate).getTime();
        long betweenDay = (endTime - beginTime) / 86400000L + 1L;
        return (int)betweenDay;
    }

    public static int getBetweenDay(java.util.Date bdate, java.util.Date edate)
            throws ParseException
    {
        return Integer.parseInt(String.valueOf((edate.getTime() - bdate.getTime()) / 86400000L)) + 1;
    }

    public static String getBeginDate(String yearMonth, int point)
    {
        if ((yearMonth == null) || (yearMonth.length() != 6)) {
            throw new IllegalArgumentException("传入的年月格式不正确：\"" + yearMonth + "\"");
        }

        int year = Integer.parseInt(yearMonth.substring(0, 4));
        int month = Integer.parseInt(yearMonth.substring(4, 6));

        if ((year <= 0) || (month <= 0) || (month > 12)) {
            throw new IllegalArgumentException("传入的年月格式不正确：\"" + yearMonth + "\"");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, point);
        calendar.add(2, -1);
        calendar.add(5, 1);

        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    public static String getEndDate(String yearMonth, int point)
    {
        if ((yearMonth == null) || (yearMonth.length() != 6)) {
            throw new IllegalArgumentException("传入的年月格式不正确：\"" + yearMonth + "\"");
        }

        int year = Integer.parseInt(yearMonth.substring(0, 4));
        int month = Integer.parseInt(yearMonth.substring(4, 6));

        if ((year <= 0) || (month <= 0) || (month > 12)) {
            throw new IllegalArgumentException("传入的年月格式不正确：\"" + yearMonth + "\"");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, point);

        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    public static Long getXcXs(java.util.Date date, java.util.Date mydata)
    {
        Long xs = Long.valueOf((mydata.getTime() - date.getTime()) / 3600000L);

        return xs;
    }

    public static java.util.Date addDateByHouse(java.util.Date date, int hours)
    {
        return addDateByFidld(date, 10, hours);
    }

    public static java.util.Date addDateByYear(java.util.Date date, int year)
    {
        return addDateByFidld(date, 1, year);
    }

    public static java.util.Date addDateByMonth(java.util.Date date, int month) {
        return addDateByFidld(date, 2, month);
    }

    public static java.util.Date addDateByDay(java.util.Date date, int day)
    {
        return addDateByFidld(date, 11, day * 24);
    }

    public static java.util.Date addDateByMinute(java.util.Date date, int minute) {
        return addDateByFidld(date, 12, minute);
    }

    public static java.util.Date addDateBySecond(java.util.Date date, int second) {
        return addDateByFidld(date, 13, second);
    }

    public static java.util.Date addDateByFidld(java.util.Date date, int field, int num)
    {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        ca.add(field, num);
        return ca.getTime();
    }

    public static java.util.Date getDateByAge(int age)
    {
        java.util.Date date = new java.util.Date();
        java.util.Date newDate = addDateByYear(date, -age);
        return newDate;
    }

    public static String pyDate(String date, int py)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String str = "";
        try {
            java.util.Date parse = sdf.parse(date);
            Calendar ca = Calendar.getInstance();
            ca.setTime(parse);
            ca.add(11, py);
            parse = ca.getTime();
            str = sdf.format(parse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String timeBetween(java.util.Date date1, java.util.Date date2)
    {
        if ((date1 == null) || (date2 == null)) return "";
        String resultStr = "";
        long miaocount = 0L;
        long mincount = 0L;
        long hourCount = 0L;
        long dayCount = 0L;
        long DAY = 86400000L;
        long HOUR = 3600000L;
        long MIN = 60000L;
        long MIAO = 1000L;
        long haomiao = 0L;
        long miaolen = date2.getTime() - date1.getTime();
        if (miaolen < 1000L) haomiao = miaolen;
        if (miaolen > 60L) miaocount = miaolen / MIAO % 60L;
        mincount = miaolen / MIN % 60L;
        hourCount = miaolen / HOUR % 24L;
        dayCount = miaolen / DAY;
        resultStr = dayCount + "天" + hourCount + "小时" + mincount + "分" + miaocount + "秒";
        if (dayCount == 0L)
            resultStr = hourCount + "小时" + mincount + "分" + miaocount + "秒";
        if ((dayCount == 0L) && (hourCount == 0L))
            resultStr = mincount + "分" + miaocount + "秒";
        if ((dayCount == 0L) && (hourCount == 0L) && (mincount == 0L))
            resultStr = miaocount + "秒";
        if ((dayCount == 0L) && (hourCount == 0L) && (mincount == 0L) && (miaocount == 0L)) {
            resultStr = haomiao + "毫秒";
        }
        return resultStr;
    }

    public static java.util.Date parseDate(String dateValue)
    {
        return parseDate(dateValue, "yyyy-MM-dd HH:mm:ss");
    }

    public static java.util.Date parseDate(String dateValue, String strFormat)
    {
        if (dateValue == null) {
            return null;
        }
        if (Pattern.matches("\\d{4}-\\d{2}-\\d{2}[T|\\s]\\d{2}:\\d{2}:\\d{2}.\\d{1,3}Z?", dateValue)) {
            dateValue = dateValue.replace("T", " ");
            dateValue = dateValue.split("\\.")[0];
        }
        if (strFormat == null) {
            strFormat = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(strFormat);
        java.util.Date newDate = null;
        try {
            newDate = dateFormat.parse(dateValue);
        } catch (ParseException e) {
            newDate = null;
        }
        return newDate;
    }

    public static String DataDateFormat(String str, String formatStr)
    {
        if ((str.length() == 24) && (str.indexOf("T") == 10) && (str.indexOf("Z") == 23))
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            try {
                format.setLenient(false);
                java.util.Date date = format.parse(str);
                if (formatStr == null) {
                    formatStr = "yyyy-MM-dd HH:mm:ss.SSS";
                }
                str = formatDate(date, formatStr);
            } catch (ParseException e) {
                str = "";
            }
        }
        return str;
    }

    public static java.util.Date parseDateByUDBDateStr(String str)
    {
        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            format.setLenient(false);
            return format.parse(str); } catch (ParseException e) {
        }
        return null;
    }

    public static String addTZFormat(String str, String formatStr)
    {
        if (formatStr == null) {
            formatStr = "yyyy-MM-dd HH:mm:ss";
        }
        java.util.Date date = parseDate(str, formatStr);

        str = formatDate(date, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return str;
    }

    public static String dateToTZFormat(java.util.Date date)
    {
        String string = formatDate(date, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return string;
    }

    public static String DateFormatTZ(java.util.Date date)
    {
        String string = "";

        string = formatDate(date, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return string;
    }

    public static String format(java.util.Date aTs_Datetime, String as_Pattern)
    {
        if ((aTs_Datetime == null) || (as_Pattern == null)) {
            return null;
        }
        SimpleDateFormat dateFromat = new SimpleDateFormat();
        dateFromat.applyPattern(as_Pattern);
        return dateFromat.format(aTs_Datetime);
    }

    public static java.util.Date isValidDate(String str)
    {
        java.util.Date flag = null;
        String geshi = "yyyy/MM/dd HH:mm:ss";
        if (str.indexOf("-") > 0) geshi = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat format = new SimpleDateFormat(geshi);
        format.setLenient(false);
        try {
            flag = format.parse(str);
        } catch (ParseException e) {
            flag = null;
        }
        return flag;
    }

    public static String longToDate(String dateFormat, Long millSec)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        java.util.Date date = longToDate(millSec);
        if (dateFormat == null) {
            dateFormat = "yyyy-MM-dd HH:mm:ss";
        }
        return sdf.format(date);
    }

    public static java.util.Date longToDate(Long millSec)
    {
        Calendar ca = Calendar.getInstance();
        java.util.Date date = new java.util.Date(millSec.longValue());
        ca.setTime(date);
        ca.add(11, -8);
        return ca.getTime();
    }

    public static int getMaxDayForMonth(java.util.Date date)
    {
        java.util.Date nextMonth = addDateByMonth(date, 1);
        java.util.Date maxDay = addDateByDay(nextMonth, -1);

        Calendar ca = Calendar.getInstance();
        ca.setTime(maxDay);
        return ca.get(5);
    }

    public static int getNowMonth()
    {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(2) + 1;
        return currentMonth;
    }

    public static int getNowJidu()
    {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(2) + 1;
        if ((currentMonth >= 1) && (currentMonth <= 3))
            return 1;
        if ((currentMonth >= 4) && (currentMonth <= 6))
            return 2;
        if ((currentMonth >= 7) && (currentMonth <= 9)) {
            return 3;
        }
        return 4;
    }

    public static int getJiduByDate(java.util.Date date)
    {
        Calendar c = dateToCalendar(date);
        int currentMonth = c.get(2) + 1;
        if ((currentMonth >= 1) && (currentMonth <= 3))
            return 1;
        if ((currentMonth >= 4) && (currentMonth <= 6))
            return 2;
        if ((currentMonth >= 7) && (currentMonth <= 9)) {
            return 3;
        }
        return 4;
    }

    public static String getJiduStrByDate(java.util.Date date)
    {
        Calendar c = dateToCalendar(date);
        int currentMonth = c.get(2) + 1;
        String jStr = "";
        if ((currentMonth >= 1) && (currentMonth <= 3))
            jStr = "第一季度";
        else if ((currentMonth >= 4) && (currentMonth <= 6))
            jStr = "第二季度";
        else if ((currentMonth >= 7) && (currentMonth <= 9))
            jStr = "第三季度";
        else {
            jStr = "第四季度";
        }
        String suStr = format(date, "yyyy") + "年" + jStr;
        return suStr;
    }

    public static Calendar dateToCalendar(java.util.Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static java.util.Date calendarToDate(Calendar calendar)
    {
        java.util.Date date = calendar.getTime();
        return date;
    }

    public static java.util.Date getCurrentWeekDayStartTime()
    {
        Calendar c = dateToCalendar(new java.util.Date());
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int weekday = c.get(7) - 2;
            c.add(5, -weekday);
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c.getTime();
    }

    public static java.util.Date getCurrentWeekDayEndTime()
    {
        Calendar c = dateToCalendar(new java.util.Date());
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int weekday = c.get(7);
            c.add(5, 8 - weekday);
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c.getTime();
    }

    public static java.util.Date getCurrentDayStartTime()
    {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date now = new java.util.Date();
        try {
            now = shortSdf.parse(shortSdf.format(now));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentDayEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date now = new java.util.Date();
        try {
            now = longSdf.parse(shortSdf.format(now) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentHourStartTime()
    {
        SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");
        java.util.Date now = new java.util.Date();
        try {
            now = longHourSdf.parse(longHourSdf.format(now));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentHourEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");
        java.util.Date now = new java.util.Date();
        try {
            now = longSdf.parse(longHourSdf.format(now) + ":59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentMonthStartTime()
    {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        java.util.Date now = null;
        try {
            c.set(5, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentMonthEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        java.util.Date now = null;
        try {
            c.set(5, 1);
            c.add(2, 1);
            c.add(5, -1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentYearStartTime()
    {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        java.util.Date now = null;
        try {
            c.set(2, 0);
            c.set(5, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentYearEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        java.util.Date now = null;
        try {
            c.set(2, 11);
            c.set(5, 31);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentQuarterStartTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        int currentMonth = c.get(2) + 1;
        java.util.Date now = null;
        try {
            if ((currentMonth >= 1) && (currentMonth <= 3))
                c.set(2, 0);
            else if ((currentMonth >= 4) && (currentMonth <= 6))
                c.set(2, 3);
            else if ((currentMonth >= 7) && (currentMonth <= 9))
                c.set(2, 4);
            else if ((currentMonth >= 10) && (currentMonth <= 12))
                c.set(2, 9);
            c.set(5, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getCurrentQuarterEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        int currentMonth = c.get(2) + 1;
        java.util.Date now = null;
        try {
            if ((currentMonth >= 1) && (currentMonth <= 3)) {
                c.set(2, 2);
                c.set(5, 31);
            } else if ((currentMonth >= 4) && (currentMonth <= 6)) {
                c.set(2, 5);
                c.set(5, 30);
            } else if ((currentMonth >= 7) && (currentMonth <= 9)) {
                c.set(2, 8);
                c.set(5, 30);
            } else if ((currentMonth >= 10) && (currentMonth <= 12)) {
                c.set(2, 11);
                c.set(5, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getHalfYearStartTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        int currentMonth = c.get(2) + 1;
        java.util.Date now = null;
        try {
            if ((currentMonth >= 1) && (currentMonth <= 6))
                c.set(2, 0);
            else if ((currentMonth >= 7) && (currentMonth <= 12)) {
                c.set(2, 6);
            }
            c.set(5, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static java.util.Date getHalfYearEndTime()
    {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = dateToCalendar(new java.util.Date());
        int currentMonth = c.get(2) + 1;
        java.util.Date now = null;
        try {
            if ((currentMonth >= 1) && (currentMonth <= 6)) {
                c.set(2, 5);
                c.set(5, 30);
            } else if ((currentMonth >= 7) && (currentMonth <= 12)) {
                c.set(2, 11);
                c.set(5, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }

    public static List<String> getMonthBetween(java.util.Date minDate, java.util.Date maxDate)
    {
        List result = new ArrayList();

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(minDate);
        min.set(min.get(1), min.get(2), 1);

        max.setTime(maxDate);
        max.set(max.get(1), max.get(2), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(format(curr.getTime(), "yyyy-MM"));
            curr.add(2, 1);
        }
        return result;
    }

    public static List<String> getDaysBetween(java.util.Date dateOne, java.util.Date dateTwo)
    {
        List result = new ArrayList();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateOne);
            while (calendar.getTime().before(dateTwo)) {
                result.add(DATE_FORMAT_YYYY_MM_DD.format(calendar.getTime()));
                calendar.add(5, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static long hmsConvertMillisAll(String times)
    {
        SimpleDateFormat sdf = DATE_FORMAT_YYYY_MM_DDkgHHmhMMmmss;
        long time = 0L;
        try {
            time = sdf.parse(times).getTime();
        } catch (ParseException e) {
            System.out.println("错误代码是：" + e);
            e.printStackTrace();
        }
        return time;
    }

    public static java.util.Date timestampToDate(Timestamp timestamp)
    {
        java.util.Date date = new java.util.Date(timestamp.getTime());
        return date;
    }

   /* public static <T> T clearUDBDate(T t)
    {
        if (t != null) {
            List mapList = null;
            if ((t instanceof Map)) {
                mapList = new ArrayList();
                mapList.add((Map)t);
            } else if ((t instanceof List)) {
                mapList = (List)t;
            }
            if (mapList != null)
            {
                Iterator keyIter;
                for (Iterator localIterator1 = mapList.iterator(); localIterator1.hasNext();
                     keyIter.hasNext())
                {
                    Map map = (Map)localIterator1.next();
                    keyIter = map.keySet().iterator();
                    continue;
                    String key = (String)keyIter.next();
                    Object v = map.get(key);
                    if ((v != null) &&
                            (Pattern.matches("\\d{4}-\\d{2}-\\d{2}[T|\\s]\\d{2}:\\d{2}:\\d{2}.\\d{1,3}Z?", v.toString()))) {
                        String vStr = v.toString();
                        vStr = vStr.replace("T", " ");
                        vStr = vStr.split("\\.")[0];
                        map.put(key, vStr);
                    }
                }
            }

        }

        return t;
    }*/

    public static void main(String[] args)
    {
        java.util.Date date = parseDate("2017-03-22 11:56:31", "yyyy-MM-dd HH:mm:ss");
        System.out.println(getCurrDate("yyyyMMddHHmmss"));
    }
}