package com.bigdata.spark.util;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * Created by dahey on 2018/3/20.
 */
public class RedisClient implements KryoSerializable
{
    private static final Logger logger = Logger.getLogger(RedisClient.class);
    private static final int TIMEOUT = 100000;
    private static String host;
    private static int port;
    private static RedisClient instance;
    private static JedisPool jedisPool;

    private RedisClient(String host, int port)
    {
        host = host;
        port = port;
        initJedisPool();
    }

    public static RedisClient getInstance(String host, int port) {
        if (instance == null) {
            synchronized (RedisClient.class) {
                if (instance == null) {
                    instance = new RedisClient(host, port);
                }
            }
        }
        return instance;
    }

    private void initJedisPool() {
        jedisPool = new JedisPool(new JedisPoolConfig(), host, port, 100000);
    }

    public Jedis getResource() {
        if (jedisPool == null) {
            initJedisPool();
        }
        return jedisPool.getResource();
    }

    public void returnBrokenResource(Jedis jedis) {
        if ((jedisPool != null) && (jedis != null))
            jedisPool.returnBrokenResource(jedis);
    }

    public void returnResource(Jedis jedis)
    {
        if ((jedisPool != null) && (jedis != null))
            jedisPool.returnResource(jedis);
    }

    public void write(Kryo kryo, Output output)
    {
        kryo.writeObject(output, host);
        kryo.writeObject(output, Integer.valueOf(port));
    }
    public void read(Kryo kryo, Input input) {
        host = (String)kryo.readObject(input, String.class);
        port = ((Integer)kryo.readObject(input, Integer.class)).intValue();
        jedisPool = new JedisPool(new JedisPoolConfig(), host, port, 100000);
    }
}
