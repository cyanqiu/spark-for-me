package com.bigdata.spark.util;

/**
 * Created by dahey on 2018/3/20.
 */
public class FtpFile {
    private String gjlx;
    private String fileName;

    public String getGjlx()
    {
        return this.gjlx;
    }
    public void setGjlx(String gjlx) {
        this.gjlx = gjlx;
    }
    public String getFileName() {
        return this.fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
