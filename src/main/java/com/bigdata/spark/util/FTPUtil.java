package com.bigdata.spark.util;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dahey on 2018/3/21.
 */
public class FTPUtil {
    private static Logger log = Logger.getLogger(FTPUtil.class);
    private static FTPUtil instance;
    private static final String DATA_DIR = "hzhz/";
    private FTPClient ftpClient = null;

    public static FTPUtil getInstance()
    {
        if (instance == null) {
            synchronized (FTPUtil.class) {
                if (instance == null) {
                    instance = new FTPUtil();
                }
            }
        }

        return instance;
    }

    public boolean open(String server, int port, String userName, String password)
    {
        if ((this.ftpClient != null) && (this.ftpClient.isConnected()))
            return true;
        try
        {
            this.ftpClient = new FTPClient();

            this.ftpClient.connect(server, port);

            this.ftpClient.setDataTimeout(60000);

            if (FTPReply.isPositiveCompletion(this.ftpClient.getReplyCode()))
            {
                boolean loged = this.ftpClient.login(userName, password);
                if (loged)
                {
                    this.ftpClient.setFileType(2);

                    this.ftpClient.enterLocalPassiveMode();
                } else {
                    return false;
                }
            }
            return true;
        }
        catch (Exception e) {
            close();
            e.printStackTrace();
        }return false;
    }

    public boolean mkDir(String ftpPath)
    {
        if (!this.ftpClient.isConnected())
            return false;
        try
        {
            char[] chars = ftpPath.toCharArray();
            StringBuffer sbStr = new StringBuffer(256);
            for (int i = 0; i < chars.length; i++) {
                if ('\\' == chars[i])
                    sbStr.append('/');
                else {
                    sbStr.append(chars[i]);
                }
            }
            ftpPath = sbStr.toString();
            if (ftpPath.indexOf('/') == -1)
            {
                this.ftpClient.makeDirectory(new String(ftpPath.getBytes(), "iso-8859-1"));
                this.ftpClient.changeWorkingDirectory(new String(ftpPath.getBytes(), "iso-8859-1"));
            }
            else {
                String[] paths = ftpPath.split("/");
                for (int i = 0; i < paths.length; i++) {
                    this.ftpClient.makeDirectory(new String(paths[i].getBytes(), "iso-8859-1"));
                    this.ftpClient.changeWorkingDirectory(new String(paths[i].getBytes(), "iso-8859-1"));
                }
            }
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }return false;
    }

    public void close()
    {
        try
        {
            if ((this.ftpClient != null) && (this.ftpClient.isConnected()))
                this.ftpClient.disconnect();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void writeFile(String fileName, String directory, String content) {
        InputStream is = null;
        try {
            mkDir("hzhz/" + File.separator + directory);
            content = content + System.getProperty("line.separator");
            is = new ByteArrayInputStream(content.getBytes());
            this.ftpClient.setFileType(2);
            this.ftpClient.setRestartOffset(1L);
            this.ftpClient.storeFile(new String(fileName.getBytes("utf-8"), "ISO8859-1"), is);
            cd("/");
        }
        catch (IOException e) {
            e.printStackTrace();
            try
            {
                if (is != null)
                    is.close();
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        finally
        {
            try
            {
                if (is != null)
                    is.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean cd(String dir)
    {
        try
        {
            return this.ftpClient.changeWorkingDirectory(dir);
        }
        catch (Exception e) {
            e.printStackTrace();
        }return false;
    }

    public static void main(String[] args)
    {
        for (int i = 0; i < 100; i++) {
            FTPUtil u = getInstance();
            u.open("10.64.9.207", 18021, "ftpguiji", "Scguiji@20170929#");
            u.writeFile("test.text", "/mhcg/", i + "--测试");
            u.close();
        }
    }
}
