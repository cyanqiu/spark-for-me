package com.bigdata.spark;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.json.JSONException;

import java.util.*;

public class Test {
	private static final int FTP_DATA_SIZE = 100;
	
	public static void main(String[] args) {
		String brokers = "node32.chinacloud.demo:9092";
		String topics = "number";
		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("streaming-test");
//		SparkConf conf = new SparkConf().setMaster("spark://node32.chinacloud.demo:7078").setAppName("streaming-test");
		conf.set("spark.streaming.kafka.consumer.poll.ms", "100000");
		JavaSparkContext sc = new JavaSparkContext(conf);
		sc.setLogLevel("WARN");
		
		JavaStreamingContext ssc = new JavaStreamingContext(sc,Durations.seconds(2));
		
		Collection<String> topicsSet = new HashSet<String>(Arrays.asList(topics.split(",")));
		
		Map<String, Object> kafkaParams = new HashMap<String, Object>();
		kafkaParams.put("bootstrap.servers", brokers);
		kafkaParams.put("group.id", "test");
		kafkaParams.put("auto.offset.reset","earliest");
		kafkaParams.put("key.deserializer",StringDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("enable.auto.commit",false);
		
	
		JavaInputDStream<ConsumerRecord<String, String>> stream =
				KafkaUtils.createDirectStream(
						ssc, 
						LocationStrategies.PreferConsistent(), 
						ConsumerStrategies.<String,String>Subscribe(topicsSet, kafkaParams)
						);
		
		JavaDStream<String> lines = stream.filter(new Function<ConsumerRecord<String,String>, Boolean>() {		
			@Override
			public Boolean call(ConsumerRecord<String, String> arg0) throws Exception {
				System.out.println(arg0);
				try {
					if(Integer.parseInt(arg0.value()) % 2 == 0) {
						return false;
					}
//					if(arg0 == null || arg0.value() == null) return false;
//					JSONObject kafkaData = new JSONObject(arg0.value());
				} catch (JSONException e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}
		}).map(new Function<ConsumerRecord<String,String>,String>() {

			@Override
			public String call(ConsumerRecord<String, String> arg0) throws Exception {
				return "this is "+arg0.value()+" showing";
			}
		
		});
		lines.foreachRDD(new VoidFunction<JavaRDD<String>>() {			
			@Override
			public void call(JavaRDD<String> rdd) throws Exception {
				rdd.foreachPartition(new VoidFunction<Iterator<String>>() {
					@Override
					public void call(Iterator<String> it) throws Exception {
								while (it.hasNext()) {
									String str = it.next();
									System.out.println("=========================================================");
									System.out.println(str);
								}
						
					}
				});
			}
		});
		ssc.start();
		try {
			ssc.awaitTermination();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ssc.close();
	}

}
