package com.bigdata.spark;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;



public class SendToKafka{
	private HashMap<String, String> params;
	private static SendToKafka instance = null;
	private Properties props = new Properties();
	private SendToKafka(HashMap<String, String> params){
		this.params = params;
		initProducer(); 
	}
	
	public static SendToKafka getInstance(HashMap<String, String> params){
		if (null == instance) {
            synchronized (SendToKafka.class) {
                if (null == instance) {
                    instance = new SendToKafka(params);
                }
            }
        }
        return instance;
	}
	private void initProducer(){		
		props.put("bootstrap.servers","node32.chinacloud.demo:9092" );
		props.put("client.id","tt");
		// int类型的key在kafka 0.9.0版本以上才可�?
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	}
	
	public void sendToKafka(String topic,String message)throws Exception{
		KafkaProducer<String, String> producer = KafkaProducerSingleton.getInstance(props).getKafkaProducer();
		producer.send(new ProducerRecord<String, String>(topic, "" + System.currentTimeMillis(), message)).get();
	}
	
	public static void main(String[] args) {
		HashMap<String, String> params = new HashMap<String, String>();
		String ST_WB_SWRY = "{\"ST_WB_SWRY\":[{\"YYCSDM\":\"51010610000258\",\"SWRYXM\":\"邱世贵\",\"ROWKEY\":\"51101119780120875220171017100757\",\"DZQH\":\"511321\",\"XWSJ\":\"20171017100857\",\"DEP_ACTION_TIME\":\"20171017101635\",\"SWKSSJ\":\"20171017100757\",\"ZJHM\":\"220721195811020827\",\"SWZDH\":\"073\",\"YYSMC\":\"成都市金牛区星空跨越梦想网吧\"}]}";
		String ST_ZA_GNLKXX = "{\"ST_ZA_GNLKXX\":[{\"RZFH\":\"210\",\"LDBM\":\"5107003597\",\"ROWKEY\":\"cbc069e399ef4fe55586d0abdec1c2c2\",\"SFZMHM\":\"510113199008150475\",\"DEP_ACTION_TIME\":\"20171010124515\",\"TFSJ\":\"20171010125116\",\"LDMC\":\"金桂公馆酒店\",\"RZSJ\":\"20171009182133\",\"XM\":\"程世集\"}]}";
		String ST_JJ_KKXX = "{\"ST_JJ_KKXX\":[{\"ROWKEY\":\"d49dee4a0b10d68a837351bd31256a09\",\"CTHPHM\":\"川AJ5J74\",\"FXMC\":\"\",\"KKBH\":\"510500000000100283\",\"KKMC\":null,\"GCSJ\":\"20171010073506\",\"HPYS\":\"蓝\"}]}"; 
		String ST_MH_JGXXB = "{\"ST_MH_JGXXB\":[{\"FLT_NUMBER\":\"8895\",\"STA_ARVETM\":\"20171010141500\",\"STA_DEPTTM\":\"20171010130500\",\"SEG_DEPT_CODE\":\"XIC\",\"FLT_AIRLCODE\":\"3U\",\"DEP_ACTION_TIME\":\"20171010125656\",\"CERT_NO\":\"510113199008150475\",\"PSR_CHNNAME\":\"胡安贵\",\"SN_ID\":\"7018834955\",\"SEG_DEST_CODE\":\"CTU\"}]}";
		String ST_ZA_XLPC_BPCCLXXB = "{\"ST_ZA_XLPC_BPCCLXXB\":[{\"PCSJ\":\"20171010122110\",\"HPHM\":\"川AJ5J74\",\"KKSZMC\":null,\"ROWKEY\":\"69E79A1555A541BC94EF5A32AF4A8822\",\"DEP_ACTION_TIME\":\"20171010123024\",\"KKMC\":null,\"KKXZQH\":null,\"KKID\":null}]}";
		String ST_ZA_XLPC_BPCRYXXB = "{\"ST_ZA_XLPC_BPCRYXXB\":[{\"PCSJ\":\"20171010122818\",\"KKSZMC\":\"广元市\",\"ROWKEY\":\"87E0F17987A34C04B0D843C02AE4D703\",\"DEP_ACTION_TIME\":\"20171010123121\",\"SFZH\":\"513027197603150011\",\"KKMC\":\"四川省广元市朝天区京昆高速七盘关公安检查站\",\"KKXZQH\":\"510800\",\"KKID\":\"29\",\"XM\":\"陈夫\"}]}";
		String ST_TL_LKSMDP = "{\"ST_TL_LKSMDP\":[{\"CC\":\"G2921\",\"ROWKEY\":\"a544bf57669531eeb949e411b9e9b364\",\"DEP_ACTION_TIME\":\"20171012150223\",\"FZ\":\"贵阳北\",\"CXH\":\"11\",\"CCRQ\":\"20171009\",\"ZJHM\":\"510113199008150475\",\"ZWH\":\"016F\",\"SPSJ\":\"20171009083534\",\"DZ\":\"深圳北\",\"XM\":\"吴璟\"}]}";
		
		Map<String,String> messageMap = new HashMap<>();
		messageMap.put("ST_WB_SWRY", ST_WB_SWRY);
		messageMap.put("ST_ZA_GNLKXX", ST_ZA_GNLKXX);
		messageMap.put("ST_JJ_KKXX", ST_JJ_KKXX);
		messageMap.put("ST_MH_JGXXB", ST_MH_JGXXB);
		messageMap.put("ST_ZA_XLPC_BPCCLXXB", ST_ZA_XLPC_BPCCLXXB);
		messageMap.put("ST_ZA_XLPC_BPCRYXXB", ST_ZA_XLPC_BPCRYXXB);
		messageMap.put("ST_TL_LKSMDP", ST_TL_LKSMDP);
		
		String topics [] = {"ST_WB_SWRY"};
		int total = 1000;
		for(int i = 0;i < total;i++){
			try {
				for(int j = 0;j < topics.length;j++){
					Thread.currentThread().sleep(200);
					SendToKafka.getInstance(params).sendToKafka("number",String.valueOf(i));
//					SendToKafka.getInstance(params).sendToKafka(topics[j], messageMap.get(topics[j]));
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			};
		}
		System.out.println("-----------end");
		
	}
}
