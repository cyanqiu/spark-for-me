package com.bigdata.spark;

import java.util.HashMap;
/**
 * Created by dahey on 2018/3/20.
 */
public class TypeConsumerHandlerThread {
    private final ConsumerHandler consumerHandler;

    public TypeConsumerHandlerThread(HashMap<String, String> params, String sendType, String srcTopic, String dstTopic, String message, SendToKafka sendToKafka)
    {
        if (sendType.equals("kafka")) {
            this.consumerHandler = new KafkaHandler(params, dstTopic, message, sendToKafka);
        }
        else if (sendType.equals("ftp")) {
            this.consumerHandler = new FtpHandler(params, dstTopic, message);
        }
        else
            this.consumerHandler = null;
    }

    public Long run()
            throws Exception
    {
        return this.consumerHandler.coreHandler();
    }
}
