package com.bigdata.spark.test;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author cyq
 * @date 2018/06/06
 **/

public class test1 {
    public static void main(String[] args) {
        SparkSession sparkSession=SparkSession.builder()
                .appName("daben")
                .master("local[1]")
                .getOrCreate();
        //读取txt文件
        Dataset dataset =sparkSession.read().textFile("D:\\TestFile\\test.txt");


    }
    // map
    public static void map(Dataset dataset){
        //map 每一行数据就是一个对象 计算每一行有多少个单词
        Dataset t1 = dataset.map(new MapFunction() {
            @Override
            public Object call(Object o) throws Exception {
                List<Object> list =Arrays.asList(o.toString().split(" "));
                return list.size();
            }
        }, Encoders.INT());
        t1.show();
    }

    // flatMap
    public static void flatMap(Dataset dataset){
        Dataset t2 = dataset.flatMap(new FlatMapFunction() {
            @Override
            public Iterator call(Object o) throws Exception {
                return Arrays.asList(o.toString().split(" ")).iterator();
            }
        },Encoders.STRING());

        t2.show();
    }

}
