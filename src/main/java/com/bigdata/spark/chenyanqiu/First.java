package com.bigdata.spark.chenyanqiu;


import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author cyq
 * @date 2018/06/06
 *
 * spark Streaming java 版本
 **/

public class First {
    public static void main(String[] args) {
        SparkConf sparkConf = new SparkConf().setAppName("daben").setMaster("local[1]");
        JavaStreamingContext jsc =new JavaStreamingContext(sparkConf, Durations.seconds(10));
        JavaReceiverInputDStream<String> lines = jsc.socketTextStream("localhost",9999);
        JavaDStream<String>  words = lines.flatMap(r -> Arrays.asList(r.split(" ")).iterator());
        JavaPairDStream<String, Integer> pairs = words.mapToPair(s -> new Tuple2<>(s, 1));
        JavaPairDStream<String, Integer> wordCounts = pairs.reduceByKey((i1, i2) -> i1 + i2);
        wordCounts.print();
        jsc.start();
        try {
            jsc.awaitTerminationOrTimeout(40*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
