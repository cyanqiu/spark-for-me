package com.bigdata.spark;

import org.apache.log4j.Logger;

import java.util.HashMap;
/**
 * Created by dahey on 2018/3/21.
 */
public class KafkaHandler extends ConsumerHandler{
    Logger logger = Logger.getLogger(KafkaHandler.class);
    protected SendToKafka sendToKafka;

    public KafkaHandler(HashMap<String, String> params, String dstTopic, String message, SendToKafka sendToKafka)
    {
        super(params, dstTopic, message);
        this.sendToKafka = sendToKafka;
    }

    protected Long coreHandler() {
        try {
            this.sendToKafka.sendToKafka(this.dstTopic, this.message.toString());
        } catch (Exception e) {
            e.printStackTrace();
            this.logger.warn("消息分发kafka报错!");
            return Long.valueOf(0L);
        }

        return Long.valueOf(1L);
    }
}
