package com.bigdata.spark;

import java.util.HashMap;

/**
 * Created by dahey on 2018/3/20.
 */
public abstract class ConsumerHandler {
    protected HashMap<String, String> params;
    protected String message;
    protected String dstTopic;

    public ConsumerHandler(HashMap<String, String> params, String dstTopic, String message)
    {
        this.params = params;
        this.message = message;
        this.dstTopic = dstTopic;
    }

    protected abstract Long coreHandler();
}
