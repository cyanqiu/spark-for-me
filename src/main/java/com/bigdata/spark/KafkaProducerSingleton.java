package com.bigdata.spark;

import org.apache.kafka.clients.producer.KafkaProducer;


import java.util.Properties;

/**
 * Created by patrick on 2016/3/1.
 */
public class KafkaProducerSingleton{
	private Properties properties;
	private KafkaProducer<String, String> kafkaProducer;
    private static KafkaProducerSingleton instance = null;

    private KafkaProducerSingleton(Properties properties){
    	this.properties = properties;
    	initKafkaProducer();
    }
    public void initKafkaProducer() {
        if (kafkaProducer == null) {
        	kafkaProducer = new KafkaProducer<String, String>(properties);
        }
    }

    public static KafkaProducerSingleton getInstance(Properties properties){
    	if (null == instance) {
            synchronized (KafkaProducerSingleton.class) {
                if (null == instance) {
                    instance = new KafkaProducerSingleton(properties);
                }
            }
        }
        return instance;
    }
    
    public KafkaProducer<String, String> getKafkaProducer(){
    	return kafkaProducer;
    }
}
